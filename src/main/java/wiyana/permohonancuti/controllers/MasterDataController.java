package wiyana.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import wiyana.permohonancuti.dtos.UserLeaveRequestsDto;
import wiyana.permohonancuti.models.UserLeaveRequests;
import wiyana.permohonancuti.repositories.UserLeaveRequestsRepository;

@RestController
@RequestMapping("/api/master-data")
public class MasterDataController {
	
	@Autowired
	UserLeaveRequestsRepository leaveReqRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/readall")
	public Map<String, Object> getAllUserLeaveRequestDataByPage(
			@RequestParam(name = "page") Integer page,
			@RequestParam(name = "size") Integer size,
			@RequestParam(name = "status") String status){
		/*  
		 *  ____________________
		 * | Note  				|
		 * |--------------------|
		 * | Status :- approved	|
		 * |   		 - declined	|
		 * |	 	 - waiting	|
		 * |		 - all		|
		 * |____________________|
		 */
		
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserLeaveRequestsDto> listDataDto = new ArrayList<UserLeaveRequestsDto>();
		Pageable pageable = PageRequest.of(page, size);
		
		validateStatus(status, result, listDataDto, pageable);
		
		return result;
		
	}

	private void validateStatus(String status, Map<String, Object> result, List<UserLeaveRequestsDto> listDataDto,
			Pageable pageable) {
		validateStatusNotFound(status, result);
		
		masterDataActions(status, result, listDataDto, pageable);
	}

	private void masterDataActions(String status, Map<String, Object> result, List<UserLeaveRequestsDto> listDataDto,
			Pageable pageable) {
		if(status.equalsIgnoreCase("all")) {
			
			List<UserLeaveRequests> listAllData = leaveReqRepo.findAllData(pageable);
			
			for(UserLeaveRequests leaveReqEntity : listAllData) {
				
				UserLeaveRequestsDto leaveReqDto = new UserLeaveRequestsDto();
				leaveReqDto = modelMapper.map(leaveReqEntity, UserLeaveRequestsDto.class);
				
				listDataDto.add(leaveReqDto);
				
				result.put("items", listDataDto);
				result.put("total data", listDataDto.size());
			}
		}
		else {
			List<UserLeaveRequests> listAllData = leaveReqRepo.findAllDataByStatus(status, pageable);
			
			for(UserLeaveRequests leaveReqEntity : listAllData) {
			
				UserLeaveRequestsDto leaveReqDto = new UserLeaveRequestsDto();
				leaveReqDto = modelMapper.map(leaveReqEntity, UserLeaveRequestsDto.class);
			
				listDataDto.add(leaveReqDto);
				result.put("items", listDataDto);
				result.put("total data", listDataDto.size());
			}
		}
	}

	private void validateStatusNotFound(String status, Map<String, Object> result) {
		if(	!(status.equalsIgnoreCase("all") || status.equalsIgnoreCase("approved") ||
				status.equalsIgnoreCase("declined") || status.equalsIgnoreCase("waiting"))){
			
			result.put("message", "Maaf, data dengan status tersebut tidak ditemukan.");
		}
	}
	
}
