package wiyana.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wiyana.permohonancuti.dtos.PositionsDto;
import wiyana.permohonancuti.exceptions.ResourceNotFoundException;
import wiyana.permohonancuti.models.Positions;
import wiyana.permohonancuti.repositories.PositionsRepository;

@RestController
@RequestMapping("/api/position")
public class PositionsController {
	
	@Autowired
	PositionsRepository posRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createPosition(@Valid @RequestBody PositionsDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Positions positionEntity = new Positions();
		positionEntity = modelMapper.map(body, Positions.class);
		
		posRepo.save(positionEntity);
		
		body.setPositionId(positionEntity.getPositionId());
		
		result.put("message", "create position success.");
		result.put("data", body);
		
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> getAllPosition(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Positions> listAllPositions = posRepo.findAll();
		List<PositionsDto> listPositionDto = new ArrayList<PositionsDto>();
		
		for(Positions positionEntity : listAllPositions) {
			
			PositionsDto positionDto = new PositionsDto();
			positionDto = modelMapper.map(positionEntity, PositionsDto.class);
			
			listPositionDto.add(positionDto);
		}
		
		result.put("message", "all data retrieved.");
		result.put("data", listPositionDto);
		result.put("total items", listAllPositions.size());
		
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getPosition(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Positions positionEntity = posRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Positions", "id", id));
		
		PositionsDto positionDto = new PositionsDto();
		positionDto = modelMapper.map(positionEntity, PositionsDto.class);
		
		result.put("message", "position id "+ id + " found");
		result.put("data", positionDto);
		
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePosition(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody PositionsDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Positions positionEntity = posRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Positions", "id", id));
		
		Date createdDate = positionEntity.getCreatedDate();
		String createdBy = positionEntity.getCreatedBy();
		
		body.setPositionId(positionEntity.getPositionId());
		positionEntity = modelMapper.map(body, Positions.class);
		
		positionEntity.setCreatedBy(createdBy);
		positionEntity.setCreatedDate(createdDate);
		
		posRepo.save(positionEntity);
		
		result.put("Message", "update "+id+" success.");
		result.put("data", body);
		
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePosition(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();

		Positions positionEntity = posRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Positions", "id", id));
		
		PositionsDto positionDto = new PositionsDto();
		positionDto = modelMapper.map(positionEntity, PositionsDto.class);
		
		result.put("message", "delete id "+ id + " success.");
		result.put("data", positionDto);
		
		posRepo.deleteById(id);
		
		return result;
	}
}
