package wiyana.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wiyana.permohonancuti.dtos.PositionLeavesDto;
import wiyana.permohonancuti.exceptions.ResourceNotFoundException;
import wiyana.permohonancuti.models.PositionLeaves;
import wiyana.permohonancuti.repositories.PositionLeavesRepository;

@RestController
@RequestMapping("/api/position-leaves")
public class PositionLeavesController {
	
	@Autowired
	PositionLeavesRepository posLeaveRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@Valid @RequestBody PositionLeavesDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeaves positionLeaveEntity = new PositionLeaves();
		positionLeaveEntity = modelMapper.map(body, PositionLeaves.class);
		
		posLeaveRepo.save(positionLeaveEntity);
		
		body.setPositionLeaveId(positionLeaveEntity.getPositionLeaveId());
		
		result.put("message", "create position-leave success.");
		result.put("data", body);
		
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> getPositionLeaves(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeaves> listAllPosLeaves = posLeaveRepo.findAll();
		List<PositionLeavesDto> listPosDto = new ArrayList<PositionLeavesDto>();
		
		for(PositionLeaves posLeaveEntity : listAllPosLeaves){
			
			PositionLeavesDto posLeaveDto = new PositionLeavesDto();
			posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeavesDto.class);
			
			listPosDto.add(posLeaveDto);
		}
		
		result.put("message", "all data retrieved.");
		result.put("data", listPosDto);
		result.put("total items",listAllPosLeaves.size());
		
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getPositionLeave(@PathVariable(value = "id")Long id){
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeaves posLeaveEntity = posLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("PositionLeaves", "id", id));
		
		PositionLeavesDto posLeaveDto = new PositionLeavesDto();
		posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeavesDto.class);
		
		result.put("message", "position id "+ id + " found");
		result.put("data",posLeaveDto);
		
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePositionLeave(@PathVariable(value = "id")Long id, 
			@Valid @RequestBody PositionLeavesDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeaves posLeaveEntity = posLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("PositionLeaves", "id", id));
		
		Date createdDate = posLeaveEntity.getCreatedDate();
		String createdBy = posLeaveEntity.getCreatedBy();
		
		body.setPositionLeaveId(posLeaveEntity.getPositionLeaveId());
		posLeaveEntity = modelMapper.map(body, PositionLeaves.class);
		
		posLeaveEntity.setCreatedBy(createdBy);
		posLeaveEntity.setCreatedDate(createdDate);
		
		posLeaveRepo.save(posLeaveEntity);
		
		result.put("Message", "update id "+id+" success.");
		result.put("data", body);
		
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePositionLeaves(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeaves posLeaveEntity = posLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("PositionLeaves", "id", id));
		
		PositionLeavesDto posLeaveDto = new PositionLeavesDto();
		posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeavesDto.class);
		
		result.put("message", "delete id "+ id + " success.");
		result.put("data",posLeaveDto);
		
		posLeaveRepo.deleteById(id);
		
		return result;
	}
}
