package wiyana.permohonancuti.controllers;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wiyana.permohonancuti.dtos.BucketApprovalDto;
import wiyana.permohonancuti.dtos.UserLeaveRequestsDto;
import wiyana.permohonancuti.models.BucketApproval;
import wiyana.permohonancuti.models.PositionLeaves;
import wiyana.permohonancuti.models.UserLeaveRequests;
import wiyana.permohonancuti.models.Users;
import wiyana.permohonancuti.repositories.BucketApprovalRepository;
import wiyana.permohonancuti.repositories.PositionLeavesRepository;
import wiyana.permohonancuti.repositories.UserLeaveRequestsRepository;
import wiyana.permohonancuti.repositories.UsersRepository;

@RestController
@RequestMapping("/api/leave-request")
public class UserLeaveRequestsController {
	
	@Autowired
	UserLeaveRequestsRepository leaveReqRepo;
	
	@Autowired
	PositionLeavesRepository posLeaveRepo;
	
	@Autowired
	UsersRepository userRepo;
	
	@Autowired
	BucketApprovalRepository bucketRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createLeaveRequest(@Valid @RequestBody UserLeaveRequestsDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Date today = Calendar.getInstance().getTime();
		boolean tanggalSalah = false; boolean cutiBackdate = false; 
		boolean jatahCutiHabis = false; boolean jatahCutiKurang = false;
		PositionLeaves posLeaveEntity = getValidatedPositionLeave(today);
		
		UserLeaveRequests leaveReqEntity = new UserLeaveRequests();
		leaveReqEntity = modelMapper.map(body, UserLeaveRequests.class);
		leaveReqEntity.setStatusPengajuan("waiting");
		leaveReqEntity.setTanggalPengajuan(today);
		Users userEntity = userRepo.findById(body.getUsers().getUserId()).get();
		leaveReqEntity.setSisaCuti(getSisaCuti(posLeaveEntity, userEntity));
		
		validateUserLeaveRequests(body, result, tanggalSalah, cutiBackdate, jatahCutiHabis, jatahCutiKurang,
				leaveReqEntity);
		
		return result;
	}

	private long getSisaCuti(PositionLeaves posLeaveEntity, Users userEntity) {
		long sisaCuti = 0;
		if(userEntity.getPositions().getNamaPosisi().equalsIgnoreCase("Employee")) {
			sisaCuti = (posLeaveEntity.getCutiEmployee() - userEntity.getJatahCuti());
		}else if(userEntity.getPositions().getNamaPosisi().equalsIgnoreCase("Supervisor")) {
			sisaCuti = (posLeaveEntity.getCutiSupervisor() - userEntity.getJatahCuti());
		}else if(userEntity.getPositions().getNamaPosisi().equalsIgnoreCase("Staff")) {
			sisaCuti = (posLeaveEntity.getCutiStaff() - userEntity.getJatahCuti());
		}
		return sisaCuti;
	}
	
	private void validateUserLeaveRequests(UserLeaveRequestsDto body, Map<String, Object> result, boolean tanggalSalah,
			boolean cutiBackdate, boolean jatahCutiHabis, boolean jatahCutiKurang, UserLeaveRequests leaveReqEntity) {
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal.setTime(body.getLeaveDateFrom());
		cal1.setTime(body.getLeaveDateTo());
		cal2.setTime(leaveReqEntity.getTanggalPengajuan());
		
		tanggalSalah = checkTanggalSalah(tanggalSalah, cal, cal1);
		cutiBackdate = checkCutiBackdate(cutiBackdate, cal, cal1, cal2);
		long durasiCuti = getDurasiCuti(cal, cal1);
		jatahCutiKurang = checkJatahCuti(jatahCutiKurang, leaveReqEntity, durasiCuti);
		jatahCutiHabis = checkJatahCutiHabis(jatahCutiHabis, leaveReqEntity);
		
		setActions(body, result, tanggalSalah, cutiBackdate, jatahCutiHabis, jatahCutiKurang, leaveReqEntity, cal, cal1,
				durasiCuti);
	}

	private void setActions(UserLeaveRequestsDto body, Map<String, Object> result, boolean tanggalSalah,
			boolean cutiBackdate, boolean jatahCutiHabis, boolean jatahCutiKurang, UserLeaveRequests leaveReqEntity,
			Calendar cal, Calendar cal1, long durasiCuti) {
		if(tanggalSalah) {
			result.put("message", "tanggal yang anda ajukkan tidak valid.");
		}else if(cutiBackdate) {
			result.put("message", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.");
		}else if(jatahCutiKurang) {
			result.put("message", "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "+
					cal.get(Calendar.DATE)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.YEAR)+" sampai "+
					cal1.get(Calendar.DATE)+"-"+cal1.get(Calendar.MONTH)+"-"+cal1.get(Calendar.YEAR)+
					" ("+durasiCuti+" hari). Jatah cuti anda yang tersisa adalah "+leaveReqEntity.getSisaCuti()+" hari");
		}else if(jatahCutiHabis) {
			result.put("message", "Mohon maaf, jatah cuti Anda telah habis");
		}
		else {
			leaveReqRepo.save(leaveReqEntity);
			body.setLeaveRequestId(leaveReqEntity.getLeaveRequestId());
			body.setSisaCuti(leaveReqEntity.getSisaCuti());
			result.put("message", "Permohonan Anda sedang diproses.");
			result.put("data", body);
		}
	}

	private boolean checkJatahCutiHabis(boolean jatahCutiHabis, UserLeaveRequests leaveReqEntity) {
		if(leaveReqEntity.getSisaCuti() == 0) {
			jatahCutiHabis = true;
		}
		return jatahCutiHabis;
	}

	private long getDurasiCuti(Calendar cal, Calendar cal1) {
		return ChronoUnit.DAYS.between(cal.toInstant(), cal1.toInstant());
	}

	private boolean checkJatahCuti(boolean jatahCutiKurang, UserLeaveRequests leaveReqEntity, long durasiCuti) {
		if(durasiCuti > leaveReqEntity.getSisaCuti()) {
			jatahCutiKurang = true;
		}
		return jatahCutiKurang;
	}

	private boolean checkCutiBackdate(boolean cutiBackdate, Calendar cal, Calendar cal1, Calendar cal2) {
		if(cal.before(cal2) || cal1.before(cal2)) {
			cutiBackdate = true;
		}
		return cutiBackdate;
	}

	private boolean checkTanggalSalah(boolean tanggalSalah, Calendar cal, Calendar cal1) {
		if(cal.after(cal1)) {
			tanggalSalah = true;
		}
		return tanggalSalah;
	}



	private PositionLeaves getValidatedPositionLeave(Date today) {
		List<PositionLeaves> listAllPosLeaves = posLeaveRepo.findAll();
		PositionLeaves posLeaveValidated = new PositionLeaves();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		int tahunPengajuan = cal.get(Calendar.YEAR);
		for(PositionLeaves posLeaveEntity : listAllPosLeaves) {
			cal.setTime(posLeaveEntity.getMasterDate());
			int tempTahun = cal.get(Calendar.YEAR);
			
			if(tahunPengajuan == tempTahun) {
				posLeaveValidated = posLeaveEntity;
			}
		}
		return posLeaveValidated;
	}
	

	@GetMapping("/listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> getLeaveRequests(
			@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "totalDataPerPage") int size,
			@PathVariable(value = "choosenPage") int page){
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		Pageable pageable = PageRequest.of(page, size);
		
		List<UserLeaveRequests> listData = leaveReqRepo.findAllById(userId, pageable);
		List<UserLeaveRequestsDto> listDataDto = new ArrayList<UserLeaveRequestsDto>();
		
		for(UserLeaveRequests leaveReqEntity : listData) {
			
			UserLeaveRequestsDto leaveReqDto = new UserLeaveRequestsDto();
			leaveReqDto = modelMapper.map(leaveReqEntity, UserLeaveRequestsDto.class);
			
			listDataDto.add(leaveReqDto);
		}
		result.put("items", listDataDto);
		result.put("total items", listDataDto.size());
		
		return result;
	}
	
	@PostMapping("/resolve")
	public Map<String, Object> resolveRequestLeave(@Valid @RequestBody BucketApprovalDto body){
		
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserLeaveRequests> listAllLeaveReq = leaveReqRepo.findAll();
		
		BucketApproval bucketEntity = new BucketApproval();
		bucketEntity = modelMapper.map(body, BucketApproval.class);
		
		validateResolve(body, result, listAllLeaveReq, bucketEntity);
		
		return result;
	}

	private void validateResolve(BucketApprovalDto body, Map<String, Object> result,
			List<UserLeaveRequests> listAllLeaveReq, BucketApproval bucketEntity) {
		UserLeaveRequests leaveReqTemp = new UserLeaveRequests();
		long tempId = 0;
		//cek approval id
		boolean permohonanInvalid = true;
		for(UserLeaveRequests leaveReqEntity : listAllLeaveReq) {
			if(bucketEntity.getUserLeaveRequests().getLeaveRequestId() == leaveReqEntity.getLeaveRequestId()) {
				permohonanInvalid = false;
				leaveReqTemp = leaveReqEntity;
				tempId = leaveReqEntity.getLeaveRequestId();			
				}
		}
		
		//cek resolvedDate
		boolean invalidDate = checkResolvedDate(bucketEntity, leaveReqTemp);
		
		//validasi user privileges
		Users userTemp1 = userRepo.findById(body.getUsers().getUserId()).get();//yang mau acc
		UserLeaveRequests leaveReqTemp1 = leaveReqRepo.findById(body.getUserLeaveRequests().getLeaveRequestId()).get();//yg diacc 
		boolean employeeValid = checkEmployee(userTemp1);
		
		boolean supervisorValid = checkSupervisor(body, userTemp1);
		
		boolean staffValid = checkStaff(userTemp1, leaveReqTemp1);
		
		decideResolveAction(body, result, bucketEntity, leaveReqTemp, tempId, permohonanInvalid, invalidDate,
				employeeValid, supervisorValid, staffValid);
	}

	private void decideResolveAction(BucketApprovalDto body, Map<String, Object> result, BucketApproval bucketEntity,
			UserLeaveRequests leaveReqTemp, long tempId, boolean permohonanInvalid, boolean invalidDate,
			boolean employeeValid, boolean supervisorValid, boolean staffValid) {
		if(permohonanInvalid) {
			
			result.put("message", "Permohonan dengan ID "+ bucketEntity.getUserLeaveRequests().getLeaveRequestId()+
									" tidak ditemukan.");
		}else if(invalidDate) {
			
			result.put("message", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
			
		}else if(employeeValid == false) {
			result.put("message", "Maaf anda tidak memiliki hak untuk melakukan ini");
		}else if(supervisorValid == false) {
			result.put("message", "Maaf anda tidak memiliki hak untuk melakukan ini");
		}else if(staffValid == false) {
			result.put("message", "Maaf anda tidak memiliki hak untuk melakukan ini");
		}
		else {//set actions
			bucketRepo.save(bucketEntity);
			result.put("message", "Permohonan dengan ID "+leaveReqTemp.getLeaveRequestId()+" telah berhasil diputuskan.");
			body.setBucketApprovalId(bucketEntity.getBucketApprovalId());
			
			//set bucket approval id di leave req
			setBucketApproval(body, bucketEntity, leaveReqTemp, tempId);
			
			//add jatah cuti digunakan di user
			setJatahCuti(leaveReqTemp);
		}
	}

	private void setJatahCuti(UserLeaveRequests leaveReqTemp) {
		if(leaveReqTemp.getStatusPengajuan().equalsIgnoreCase("approved")) {
			Users user = userRepo.findById(leaveReqTemp.getUsers().getUserId()).get();
			Calendar from = Calendar.getInstance();
			Calendar to = Calendar.getInstance();
			from.setTime(leaveReqTemp.getLeaveDateFrom());
			to.setTime(leaveReqTemp.getLeaveDateTo());
			long cutiTerpakai = getDurasiCuti(from, to);
			long temp = user.getJatahCuti();
			user.setJatahCuti((int)(cutiTerpakai+temp));
			userRepo.save(user);
		}
	}

	private void setBucketApproval(BucketApprovalDto body, BucketApproval bucketEntity, UserLeaveRequests leaveReqTemp,
			long tempId) {
		Users userTemp = userRepo.findById(bucketEntity.getUsers().getUserId()).get();
		leaveReqTemp.setLeaveRequestId(tempId);
		leaveReqTemp.setStatusPengajuan(bucketEntity.getStatus());
		leaveReqTemp.setBucketApprovalId(body.getBucketApprovalId());
		leaveReqTemp.setResolvedBy(userTemp.getNama());
		leaveReqTemp.setResolvedDate(bucketEntity.getResolvedDate());
		leaveReqTemp.setResolverReason(bucketEntity.getResolverReason());
		leaveReqRepo.save(leaveReqTemp);
	}

	private boolean checkStaff(Users userTemp1, UserLeaveRequests leaveReqTemp1) {
		boolean staffValid = true;
		if(leaveReqTemp1.getUsers().getPositions().getNamaPosisi().equalsIgnoreCase("Staff")) {
			if(userTemp1.getPositions().getNamaPosisi().equalsIgnoreCase("Staff")) {
				staffValid = true;
			}else {
				staffValid = false;
			}
		}
		return staffValid;
	}

	private boolean checkSupervisor(BucketApprovalDto body, Users userTemp1) {
		boolean supervisorValid = true;
		if(userTemp1.getPositions().getNamaPosisi().equalsIgnoreCase("Supervisor")) {
			if(userTemp1.getUserId() == body.getUsers().getUserId()) {
				supervisorValid = false;
			}
		}
		return supervisorValid;
	}

	private boolean checkEmployee(Users userTemp1) {
		boolean employeeValid = true;
		if(userTemp1.getPositions().getNamaPosisi().equalsIgnoreCase("Employee")) {
			employeeValid = false;
		}
		return employeeValid;
	}

	private boolean checkResolvedDate(BucketApproval bucketEntity, UserLeaveRequests leaveReqTemp) {
		boolean invalidDate = false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(bucketEntity.getResolvedDate());
		
		if(cal.after(leaveReqTemp.getTanggalPengajuan()) && cal.before(leaveReqTemp.getLeaveDateFrom())) {
			invalidDate = true;
		}
		return invalidDate;
	}
}
