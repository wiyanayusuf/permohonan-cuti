package wiyana.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wiyana.permohonancuti.dtos.UsersDto;
import wiyana.permohonancuti.exceptions.ResourceNotFoundException;
import wiyana.permohonancuti.models.Users;
import wiyana.permohonancuti.repositories.PositionLeavesRepository;
import wiyana.permohonancuti.repositories.UsersRepository;

@RestController
@RequestMapping("/api/user")
public class UsersController {
	
	@Autowired
	UsersRepository userRepo;
	@Autowired
	PositionLeavesRepository posLeaveRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createUser(@Valid @RequestBody UsersDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Users userEntity = new Users();
		userEntity = modelMapper.map(body, Users.class);
		userEntity.setJatahCuti(0);
		
		userRepo.save(userEntity);
		
		body.setUserId(userEntity.getUserId());
		
		result.put("message", "create user success.");
		result.put("data", body);
		
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> getAllUsers(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Users> listAllUsers = userRepo.findAll();
		List<UsersDto> listUserDto = new ArrayList<UsersDto>();
		
		for(Users userEntity : listAllUsers) {
			
			UsersDto userDto = new UsersDto();
			userDto = modelMapper.map(userEntity, UsersDto.class);
			
			listUserDto.add(userDto);
		}
		
		result.put("message", "all data retrieved.");
		result.put("data", listUserDto);
		result.put("total items", listAllUsers.size());
		
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getUser(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));
		
		UsersDto userDto = new UsersDto();
		userDto = modelMapper.map(userEntity, UsersDto.class);
		
		result.put("message", "user id "+ id + " found");
		result.put("data", userDto);
		
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateUser(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody UsersDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));
		
		Date createdDate = userEntity.getCreatedDate();
		String createdBy = userEntity.getCreatedBy();
		
		body.setUserId(userEntity.getUserId());
		userEntity = modelMapper.map(body, Users.class);
		
		userEntity.setCreatedBy(createdBy);
		userEntity.setCreatedDate(createdDate);
		
		userRepo.save(userEntity);
		
		result.put("Message", "update success.");
		result.put("data", body);
		
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteUser(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();

		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));
		
		UsersDto userDto = new UsersDto();
		userDto = modelMapper.map(userEntity, UsersDto.class);
		
		result.put("message", "delete id "+ id + " success.");
		result.put("data", userDto);
		
		userRepo.deleteById(id);
		
		return result;
	}
	
}
