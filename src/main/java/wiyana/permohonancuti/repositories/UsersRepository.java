package wiyana.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wiyana.permohonancuti.models.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long>{

}
