package wiyana.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wiyana.permohonancuti.models.Positions;

@Repository
public interface PositionsRepository extends JpaRepository<Positions, Long>{

}
