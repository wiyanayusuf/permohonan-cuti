package wiyana.permohonancuti.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import wiyana.permohonancuti.models.UserLeaveRequests;

@Repository
public interface UserLeaveRequestsRepository extends JpaRepository<UserLeaveRequests, Long>{
	
	@Query(value = "select*from user_leave_requests s where s.user_id = :id ", nativeQuery = true)
	public List<UserLeaveRequests> findAllById(@Param("id") Long userId, Pageable pageable);
	
	@Query(value = "select*from user_leave_requests", nativeQuery = true)
	public List<UserLeaveRequests> findAllData(Pageable pageable);
	
	@Query(value = "select*from user_leave_requests s where s.status_pengajuan = :status", nativeQuery = true)
	public List<UserLeaveRequests> findAllDataByStatus(@Param("status") String status,Pageable pageable);
	
}
