package wiyana.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wiyana.permohonancuti.models.PositionLeaves;

@Repository
public interface PositionLeavesRepository extends JpaRepository<PositionLeaves, Long>{

}
