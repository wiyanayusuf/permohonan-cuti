package wiyana.permohonancuti.dtos;


public class PositionsDto {
	
	private long positionId;
	private String namaPosisi;
	
	public PositionsDto() {
		// TODO Auto-generated constructor stub
	}

	public PositionsDto(long positionId, String namaPosisi) {
		super();
		this.positionId = positionId;
		this.namaPosisi = namaPosisi;
	}

	public long getPositionId() {
		return positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	public String getNamaPosisi() {
		return namaPosisi;
	}

	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
}
