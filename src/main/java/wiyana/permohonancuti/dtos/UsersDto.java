package wiyana.permohonancuti.dtos;


public class UsersDto {
	
	private long userId;
	private PositionsDto positions;
	private String nama;
	private int jatahCuti;
	
	public UsersDto() {
		super();
	}

	public UsersDto(long userId, PositionsDto positions, String nama, int jatahCuti) {
		super();
		this.userId = userId;
		this.positions = positions;
		this.nama = nama;
		this.jatahCuti = jatahCuti;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public PositionsDto getPositions() {
		return positions;
	}

	public void setPositions(PositionsDto positions) {
		this.positions = positions;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getJatahCuti() {
		return jatahCuti;
	}

	public void setJatahCuti(int jatahCuti) {
		this.jatahCuti = jatahCuti;
	}
}
