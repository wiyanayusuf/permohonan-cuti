package wiyana.permohonancuti.dtos;

import java.util.Date;

public class PositionLeavesDto {
	
	private long positionLeaveId;
	private long cutiEmployee;
	private long cutiSupervisor;
	private long cutiStaff;
	private Date masterDate;
	
	public PositionLeavesDto() {
		super();
	}

	public PositionLeavesDto(long positionLeaveId, long cutiEmployee, long cutiSupervisor, long cutiStaff,
			Date createdDate, String createdBy, Date updatedDate, String updatedBy, Date masterDate) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.cutiEmployee = cutiEmployee;
		this.cutiSupervisor = cutiSupervisor;
		this.cutiStaff = cutiStaff;
		this.masterDate = masterDate;
	}

	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public long getCutiEmployee() {
		return cutiEmployee;
	}

	public void setCutiEmployee(long cutiEmployee) {
		this.cutiEmployee = cutiEmployee;
	}

	public long getCutiSupervisor() {
		return cutiSupervisor;
	}

	public void setCutiSupervisor(long cutiSupervisor) {
		this.cutiSupervisor = cutiSupervisor;
	}

	public long getCutiStaff() {
		return cutiStaff;
	}

	public void setCutiStaff(long cutiStaff) {
		this.cutiStaff = cutiStaff;
	}

	public Date getMasterDate() {
		return masterDate;
	}

	public void setMasterDate(Date masterDate) {
		this.masterDate = masterDate;
	}
	
	
}
