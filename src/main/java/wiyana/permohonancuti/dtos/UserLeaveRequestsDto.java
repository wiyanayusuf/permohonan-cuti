package wiyana.permohonancuti.dtos;

import java.util.Date;


public class UserLeaveRequestsDto {
	
	private long leaveRequestId;
	private Long bucketApprovalId;
	private UsersDto users;
	private String statusPengajuan;
	private Date tanggalPengajuan;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private long sisaCuti;
	private String resolvedBy;
	private String resolverReason;
	
	public UserLeaveRequestsDto() {
		super();
	}

	public UserLeaveRequestsDto(long leaveRequestId, Long bucketApprovalId, UsersDto users, String statusPengajuan,
			Date tanggalPengajuan, Date leaveDateFrom, Date leaveDateTo, String description, long sisaCuti,
			String resolvedBy, String resolverReason) {
		super();
		this.leaveRequestId = leaveRequestId;
		this.bucketApprovalId = bucketApprovalId;
		this.users = users;
		this.statusPengajuan = statusPengajuan;
		this.tanggalPengajuan = tanggalPengajuan;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.sisaCuti = sisaCuti;
		this.resolvedBy = resolvedBy;
		this.resolverReason = resolverReason;
	}


	public long getLeaveRequestId() {
		return leaveRequestId;
	}

	public void setLeaveRequestId(long leaveRequestId) {
		this.leaveRequestId = leaveRequestId;
	}

	
	public String getStatusPengajuan() {
		return statusPengajuan;
	}

	public void setStatusPengajuan(String statusPengajuan) {
		this.statusPengajuan = statusPengajuan;
	}

	public Date getTanggalPengajuan() {
		return tanggalPengajuan;
	}

	public void setTanggalPengajuan(Date tanggalPengajuan) {
		this.tanggalPengajuan = tanggalPengajuan;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getSisaCuti() {
		return sisaCuti;
	}

	public void setSisaCuti(long sisaCuti) {
		this.sisaCuti = sisaCuti;
	}

	public Long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(Long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
}
