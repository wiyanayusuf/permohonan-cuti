package wiyana.permohonancuti.dtos;

import java.util.Date;

public class BucketApprovalDto {
	
	private long bucketApprovalId;
	private UserLeaveRequestsDto userLeaveRequests;
	private UsersDto users;
	private Date resolvedDate;
	private String resolverReason;
	private String status;
	
	public BucketApprovalDto() {
		super();
	}

	public BucketApprovalDto(long bucketApprovalId, UserLeaveRequestsDto userLeaveRequests, UsersDto users,
			Date resolvedDate, String resolverReason, String status) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.userLeaveRequests = userLeaveRequests;
		this.users = users;
		this.resolvedDate = resolvedDate;
		this.resolverReason = resolverReason;
		this.status = status;
	}

	public long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public UserLeaveRequestsDto getUserLeaveRequests() {
		return userLeaveRequests;
	}

	public void setUserLeaveRequests(UserLeaveRequestsDto userLeaveRequests) {
		this.userLeaveRequests = userLeaveRequests;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
