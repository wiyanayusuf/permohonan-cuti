package wiyana.permohonancuti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import wiyana.permohonancuti.models.AuditorAwareImpl;

@SpringBootApplication
@EnableJpaAuditing
public class PermohonanCutiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PermohonanCutiApplication.class, args);
	}
	
	@Bean
	AuditorAware<String> auditorProvider() {
	    return new AuditorAwareImpl();
	}
	
}
